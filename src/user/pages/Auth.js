import React,{useState, useContext} from 'react';
import FacebookLogin from 'react-facebook-login';
import Card from '../../shared/components/UIElements/Card';
import Input from '../../shared/components/FormElements/Input';
import Button from '../../shared/components/FormElements/Button';
import {
  VALIDATOR_EMAIL,
  VALIDATOR_MINLENGTH,
  VALIDATOR_REQUIRE
} from '../../shared/util/validators';
import { useForm } from '../../shared/hooks/form-hook';
import { AuthContext } from '../../shared/context/auth.context';

import './Auth.css';
import { Switch } from 'react-router-dom';

const Auth = () => {
  const auth = useContext(AuthContext);
  const [isLoginMode, setIsloginMode] = useState(true);


  const [formState, inputHandler , setFormData] = useForm(
    {
      
      email: {
        value: '',
        isValid: false
      },
      password: {
        value: '',
        isValid: false
      }
    },
    false
  );

  const authSubmitHandler = event => {
    event.preventDefault();
    console.log(formState.inputs);
    auth.login();
  };

  const responseFacebook = (response) => {
    console.log(response);
    auth.login();
    
  }
   

  const switchModehandler = () => {
    if(!isLoginMode){
      console.log('in');
      setFormData(
        {
        ...formState.inputs,
        name : undefined
      }, 
      formState.inputs.email.isValid && formState.inputs.password.isValid
      )
    }else{
      console.log('out');
      setFormData({
        ...formState.inputs,
        name: {value: "", isValid: false}
      },
      false
      );
    }
    setIsloginMode(prevMode => !prevMode);
  }

  return (
    <Card className="authentication">
      <h2 className="logintext">AirSpeed</h2>
      <hr />
      <form onSubmit={authSubmitHandler}>
        {!isLoginMode && (
        <Input 
        element="input" 
        id="name" 
        type="Text" 
        placeholder ="Name"
        label="Your Name" 
        validators={[VALIDATOR_REQUIRE()]} 
        errorText="Please Enter a Name" 
        onInput ={inputHandler}
        /> 
          )}
        <Input
          element="input"
          id="email"
          type="email"
          placeholder ="E-mail"
          className="form-control"
          validators={[VALIDATOR_EMAIL()]}
          errorText="Please enter a valid email address."
          onInput={inputHandler}
        />

        <Input
          element="input"
          id="password"
          type="password"
          placeholder="Password"
          validators={[VALIDATOR_MINLENGTH(5)]}
          errorText="Please enter a valid password, at least 5 characters."
          onInput={inputHandler}
        />

        <Button type="submit"  disabled={!formState.isValid}>
          {isLoginMode ? 'LOGIN' : 'SIGNUP'}
        </Button> 

        
      </form>
      <Button className="signupbutton" inverse onClick={switchModehandler}>
          {isLoginMode ? 'SIGNUP' : 'LOGIN'}
        </Button>
      
   
        {/* facebook login */}
        {isLoginMode && (
       <FacebookLogin
       appId="257862752370798"
       autoLoad={true}
       fields="name,email,picture"
       callback={responseFacebook}/>
          )}
    </Card>
  );
};

export default Auth;
